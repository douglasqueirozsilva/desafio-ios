//
//  Repository.swift
//  AppFork
//
//  Created by Douglas Queiroz on 8/28/16.
//  Copyright © 2016 Douglas Queiroz. All rights reserved.
//

import Foundation
import ObjectMapper

class Repository: Mappable {
    var name: String?
    var description: String?
    var owner: Owner?
    var fork:Int?
    var start: Int?
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        description <- map["description"]
        owner <- map["owner"]
        fork <- map["forks_count"]
        start <- map["stargazers_count"]
    }
}