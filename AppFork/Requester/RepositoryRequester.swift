//
//  RepositoryRequester.swift
//  AppFork
//
//  Created by Douglas Queiroz on 8/28/16.
//  Copyright © 2016 Douglas Queiroz. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper


class RepositoryRequester {
    
    func getRepositories(page: Int, handle: ([Repository])->Void) {
        
        Alamofire.request(.GET, "https://api.github.com/search/repositories?q=language:Java&sort=stars&page=\(page)")
            .responseObject { (response: Response<RepositorySearch, NSError>) in
                let search = response.result.value
                
                if let search = search {
                    if let items = search.items {
                        handle(items)
                    }else{
                        handle([])
                    }
                }
        }
    }
    
    func getPullRequests(repository: Repository, handle: ([PullRequest])->Void) {
        Alamofire.request(.GET, "https://api.github.com/repos/\(repository.owner!.name!)/\(repository.name!)/pulls")
            .responseArray { (response: Response<[PullRequest], NSError>) in
                
                let pullRequests = response.result.value
                
                if let pullRequests = pullRequests {
                    handle(pullRequests)
                }else{
                    handle([])
                }
        }
        
    }
}